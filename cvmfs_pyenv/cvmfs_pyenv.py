# -*- coding: utf-8 -*-

# PyVkFFT
#   (c) 2023- : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr
#
#
# Script to install a conda environment, pip packages and create a corresponding
# environmental module in the ESRF CVMFS directories.

"""
Script to install a conda environment, pip packages and create a corresponding
environmental module in the ESRF CVMFS directories.

The conda environment will be installed in:
    /cvmfs/REPOSITORY/software/packages/DISTRIBRELEASE/ARCH/NAME/VERSION

And the environmental module in:
    /cvmfs/REPOSITORY/software/modules/DISTRIBRELEASE/ARCH/NAME/VERSION

Where e.g.:
  REPOSITORY=hpc.esrf.fr
  DISTRIBRELEASE=ubuntu20.04
  ARCH=x86_64
  NAME=pynx
  VERSION=2022.1-cuda11.2

NOTE: the configuration can also be passed entirely or partially as a YAML
file, which must be given as the first argument (see examples)
"""

"""
TODO:
* need to retry mamba command ? (failed once, same(?) command suceeded ??)
  Alternatively, use conda pack to stage install in /tmp or scratch space, then copy to
  destination ?
* add parsing from config file (yaml/...) ?
* See https://gitlab.cern.ch/lhcb-core/conda-environments and
  https://gitlab.cern.ch/lhcb-core/lbcondawrappers
* add ability to create an editable copy of an environment using e.g.:
  https://gitlab.cern.ch/lhcb-core/lbcondawrappers/-/blob/master/src/LbCondaWrappers/__init__.py#L484
"""

import argparse
import time
import subprocess
import os
import sys
import re
import platform
import logging
import traceback
import shutil
from io import StringIO, TextIOBase
from urllib.parse import urlparse
from tempfile import TemporaryDirectory
import yaml


class DeltaTimeFormatter(logging.Formatter):
    start_time = time.time()

    def format(self, record):
        record.delta = "%+8.2fs" % (time.time() - self.start_time)
        return super().format(record)


def new_logger_stream(logger_name: str, level: int = logging.INFO, flush_to=None, stdout=True):
    """Create / access a logger with a new StringIO output, allowing
    to share log between multiple processes using stream.getvalue().
    :param logger_name: the logger name
    :param level: the logging level
    :param flush_to: an opened file object to fluch the previous stream to, if any
    :param stdout: if True, also output log to stdout
    :return: a tuple (logger, stream)
    """
    logger = logging.getLogger(logger_name)
    if isinstance(flush_to, TextIOBase) and len(logger.handlers):
        h = logger.handlers[-1]
        h.flush()
        flush_to.write(h.stream.getvalue())
        flush_to.flush()
    logger.handlers.clear()
    log_format = '%(asctime)s-%(delta)s-%(levelname)8s: %(message)s'
    fmt = DeltaTimeFormatter(log_format)

    if stdout:
        h = logging.StreamHandler(sys.stdout)
        h.setFormatter(fmt)
        logger.addHandler(h)
    logger.setLevel(level)
    stream = StringIO()
    handler = logging.StreamHandler(stream)
    handler.setFormatter(fmt)
    logger.addHandler(handler)
    return logger, stream


epilog = """Example usage:       
cvmfs_pyenv.py --name pynx --version devel-cu11.7 --prereq cuda/11.7 --maintainer favre@esrf.fr --python 3.10 --conda-install pip wheel numpy \
cython scipy=1.9.3 matplotlib fabio ipympl pytools \
scikit-image silx ipython notebook h5py hdf5plugin ipywidgets psutil scikit-learn mako \
mpi4py pyopengl jupyterlab ipyvolume ipyfilechooser xraydb pyqt pythreejs=2.3.0 --pip-install h5glance pycuda \
pyvkfft nabu tomoscan xrayutilities pyopencl http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-devel-nightly.tar.bz2 --prereq cuda/11.7

cvmfs_pyenv.py  --name test_favre --version testing --maintainer favre@esrf.fr --python 3.10 \
--conda-install pip wheel numpy scipy=1.9.3 matplotlib h5py silx --pip-install pyopencl

Using a YAML file (extra arguments can be passed after the .yml/.yaml file):
cvmfs_pyenv.py config.yml --yes

with the following config.yml:
################################
name: test_favre
version: testing
maintainer: Vincent Favre-Nicolin <favre@esrf.fr>
python: 3.10
conda-install:
  - pip
  - wheel
  - numpy
  - scipy=1.9.3
  - matplotlib
  - h5py
  - silx
pip-install:
  - pyopencl
################################

It is also possible to use selectors to activate some options/packages selectively
depending on the python version, distribution release or architecture.
Note that the python version cannot use a selector, since it will be read first-
it is better to use the command-lien to give the python version.
################################
name: test_favre
version: testing
maintainer: Vincent Favre-Nicolin <favre@esrf.fr>
prereq: mamba cuda/11.7 # [p9]
prereq: mamba cuda/12.2 # [x86]
conda-install:
  - pip
  - wheel
  - numpy
  - scipy=1.9.3 # [py39 or py310 or py311]
  - scipy=1.11.4 # [py312]
  - matplotlib
  - h5py
  - silx
  - ocl-icd-system # [not osx]
  - ocl_icd_wrapper_apple # [osx]
pip-install:
  - pyopencl
################################

    """

# Careful - do not begin by a blank line or module will not be recognised
module_template = """#%Module########################################################################
##
##      Module file for {name} version = {version} 
##
##      Author: {maintainer}
##      Date: {date}
##

set version [file tail $ModulesCurrentModulefile]

module-whatis "{name} version {version}"

proc ModulesHelp {{ }} {{
    puts stderr "
Module name: {name}
Module version: {version}
Module description: {description}

Use 'module display {name}/{version}' to list the package contents.


Note: this environment can be customised with additional packages by
first loading the module, and then run e.g.:
  python -m venv --system-site-packages my_new_venv_name
  source my_new_venv_name/bin/activate
  pip install package_name
IMPORTANT: only use this with moderation (for a few packages), as
multiple additions can lead to undesired effect in case of conflicts
with already installed  packages. You can also request the 
addition of new packages (within reason) to the maintainer.
"
}}

proc ModulesDisplay {{}} {{
    puts stderr "
{package_content}
"
}}

conflict {module_conflict}

module-tag keep-loaded mamba
module load {module_load}
# Using prereq would work (instead of module-tag + module load) with unload_match_order
# prereq {module_load}

set path {root}/{repository}/software/packages/{distrib}{release}/{arch}/{name}/{version}

# Conda activate does not set library & include paths
prepend-path LD_LIBRARY_PATH $path/lib
prepend-path LIBRARY_PATH $path/lib
prepend-path CPATH $path/include

if {{ [module-info mode load] || [module-info mode switch2] }} {{
    puts stdout "conda activate $path ;"
}} elseif {{ [module-info mode remove] && ![module-info mode switch3] }} {{
    puts stdout "conda deactivate ;"
}}

"""

# Selector largely inspired by conda-build's selector
# (https://github.com/conda/conda-build/blob/24.3.0/conda_build/metadata.py#L254)
sel_pat = re.compile(r"(.+?)\s*(#.*)\[([^\[\]]+)\](?(2)[^\(\)]*)$")


def select_lines(ll, variants):
    v = []
    for l in ll:
        if l.lstrip().startswith("#"):
            continue
        m = sel_pat.match(l)
        if m:
            try:
                if eval(m.group(3), variants):
                    v.append(l)
                else:
                    print('\u0336'.join(l) + '\u0336')
            except Exception as ex:
                print(f"Could not understand selector pattern: {m.group(3)}\n"
                      f"from line:\n{l}")
                print("Known variants:")
                print("\n".join(f"{k:>12}: {v}" for k, v in variants.items()))
                raise ex
        else:
            v.append(l)
    return '\n'.join(v)


class CvmfsMkEnv:
    def __init__(self):
        self.logger, self.stream = new_logger_stream("cvmfs-mkenv", level=logging.INFO, stdout=True)
        self.init_parser()
        self.arch = subprocess.check_output("uname -m", shell=True).strip().lower().decode()

        # Test if the first argument passed is a YAML file.
        # In that case, convert the yaml to an argv command-line
        # This is a bit kludgy, but in the end the configuration goes through the
        # same parsing validation...
        if len(sys.argv) >= 2:
            if os.path.splitext(sys.argv[1])[-1] in ['.yml', '.yaml']:
                # sys.argv will be modified based on the yaml file
                self.logger.info(f"YAML file given as first input argument ({sys.argv[1]}) "
                                 f"- converting to command-line arguments")
                self.yaml2argv(open(sys.argv.pop(1)).readlines())
        self.params = self.parser.parse_args()

        if self.params.test:
            # Just parse the arguments and exit
            sys.exit(0)

        if self.params.distrib is None:
            if platform.system() == 'Darwin':
                self.distrib = 'macOS'
            else:
                self.distrib = subprocess.check_output("lsb_release -is", shell=True).strip().lower().decode()
        else:
            self.distrib = self.params.distrib

        if self.distrib == 'linux' or platform.system() == 'Darwin':
            self.release = ''
        else:
            if self.params.release is None:
                self.release = subprocess.check_output("lsb_release -rs", shell=True).strip().lower().decode()
            else:
                self.release = self.params.release

        self.paths = None
        self.tmp_dir = TemporaryDirectory()

    def yaml2argv(self, ll):
        """
        Convert the lines from a yaml file after applying selectors to exclude
        some lines, if necessary.

        :param ll: the lines from the yaml file
        :return: nothing
        """
        # #### Apply selectors, e.g. p9, x86, ub2004 ##########
        # First read the other command-line parameters
        # to know what system we are installing to
        p = self.parser.parse_args()
        if p.distrib is None:
            if platform.system() == 'Darwin':
                distrib = 'macOS'
            else:
                distrib = subprocess.check_output("lsb_release -is", shell=True).strip().lower().decode()
        else:
            distrib = p.distrib
        if 'linux' in distrib or platform.system() == 'Darwin':
            release = ''
        elif p.release is not None:
            release = p.release
        else:
            release = subprocess.check_output("lsb_release -rs", shell=True).strip().lower().decode()

        # Parse first the yaml file to see if a python version was supplied
        # (so a selector cannot be used for the python version)
        yml = yaml.safe_load('\n'.join(ll))
        if 'python' in yml:
            p.python = str(yml['python']) if yml['python'] != 3.1 else '3.10'

        # Known variants
        arch = self.arch
        variants = {f'py3{i}': f'3.{i}' in p.python for i in range(9, 13)}
        variants['p9'] = 'ppc64le' in arch
        variants['ppc64le'] = 'ppc64le' in arch
        variants['arm64'] = 'arm64' in arch
        variants['x86'] = 'x86' in arch
        variants['x86_64'] = 'x86_64' in arch
        variants['osx'] = 'Darwin' == platform.system()
        variants['linux'] = 'linux' in distrib
        # Requires python>=3.9
        # variants |= {f'ub{i}04': 'ubuntu' in release.lower() and f'{i}.04' in release for i in [20, 22, 24]}
        for i in [20, 22, 24]:
            variants[f'ub{i}04'] = 'ubuntu' in release.lower() and f'{i}.04' in release

        yml_str = select_lines(ll, variants)
        # #### End of selectors application ##########

        yml = yaml.safe_load(yml_str)
        print(yml)
        args = sys.argv
        print(args)
        for k, v in yml.items():
            print(k, v)
            if k in ['name', 'version', 'description', 'distrib', 'release',
                     'maintainer', 'repository', 'root']:
                if isinstance(v, str):
                    args += [f'--{k}', v]
                else:
                    args += [f'--{k}', str(v)]
            elif k in ['delete', 'yes', 'upgrade', 'replace', 'pip-no-cache-dir',
                       'pip-no-binary', 'set-default', 'override-channels']:
                args.append(f'--{k}')
            elif k in ['conda-install', 'conda-yaml', 'channel', 'var-install', 'prereq',
                       'conflict', 'pip-install', 'pip-install-sequential']:
                args.append(f'--{k}')
                if isinstance(v, str):
                    args.append(v)
                else:
                    args += v
            elif k in ['mamba-timeout', 'pip-timeout', 'cvmfs-timeout']:
                args += [f'--{k}', str(v)]
            elif k == 'python':
                args += [f'--{k}', str(v) if v != 3.1 else '3.10']
            else:
                raise RuntimeError(f"Did not understand keyword: {k}")
        cmd = ''
        for a in args:
            tmp = str(a)
            if any(c in tmp for c in ' <>&'):
                cmd += f' "{tmp}"'
            else:
                cmd += f' {tmp}'
        self.logger.info(f"Command-line arguments from YAML: {cmd}")
        sys.argv = args

    def init_parser(self):
        parser = argparse.ArgumentParser(prog="cvmfs-mkenv",
                                         description=__doc__,
                                         epilog=epilog,
                                         formatter_class=argparse.RawTextHelpFormatter)

        parser.add_argument("--name", action='store', type=str,
                            help="package name [required]")
        parser.add_argument("--version", action='store', type=str,
                            help="package version name, which will be used for the conda environment"
                                 "name, as well as the module name [required]")
        parser.add_argument("--description", action='store', type=str, default='',
                            help='String describing the package, e.g. --description "This module '
                                 'includes python packages for data analysis using pynx, '
                                 'with all dependencies as well as jupyter notebook and jupyterlab"')
        parser.add_argument("--delete", action='store_true',
                            help="Delete a package and the associated module version. Note that "
                                 "the maintainer name should also be supplied (as a double-check "
                                 "no mistake is done)")

        parser.add_argument("--yes", action='store_true',
                            help="Do not ask for confirmation when installing or updating a "
                                 "repository. Does not work for --delete.")

        group = parser.add_argument_group("Installation arguments")
        group.add_argument("--python", action='store', type=str, default='3.11',
                           choices=['3.7', '3.8', '3.9', '3.10', '3.11', '3.12'],
                           help="Python version")
        group.add_argument("--conda-install", action='store', type=str, nargs='+',
                           help='Conda packages to install - this will be passed to mamba and '
                                'can used the same syntax with specific version e.g. scipy=1.9.3 , '
                                '"scipy<1.10" (note the quotes)')
        group.add_argument("--conda-yaml", action='store', type=str, nargs='+',
                           help="Conda YAML environment file with the list of conda and optionally"
                                "pip packages and channels to install. This will be passed to "
                                "mamba using --file *. Multiple files can be included e.g. "
                                "'--conda-yaml env1.yml env2.yml etc..'.")
        group.add_argument("--upgrade", action='store_true',
                           help="Use this parameter to enable updating an environment - in this "
                                "case only the packages to be upgraded should be supplied. "
                                "'mamba update' and/or 'pip install --upgrade' will be used."
                                "Thais can also be used to add new packages to existing "
                                "environments.")
        group.add_argument("--replace", action='store_true',
                           help="If given, any pre-existing environment and module will be replaced. "
                                "Without this or --upgrade, the installation will be aborted")
        group.add_argument("--pip-install", action='store', type=str, nargs='+',
                           help="Pypi packages to install - this will be passed to pip and "
                                "can used the same syntax with specific version e.g. scipy=0.19 or "
                                "git+https://gitlab.esrf.fr/favre/wipow.git or "
                                "https://github.com/diffpy/pyobjcryst/archive/refs/tags/v2.2.6.tar.gz ."
                                "It is also possible to use http urls to tarballs e.g. "
                                "http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-devel-nightly.tar.bz2 ,"
                                "in which case the tar.gz or tar.bz2 will be pre-downloaded to "
                                "be installed by pip (which only supports https)."
                                "Finally, it is also possible to give the file path to the source "
                                "of a python package.")
        group.add_argument("--pip-install-sequential", action='store', type=str, nargs='+',
                           help='list of pip commands, which will be executed consecutively. This '
                                'can be used e.g. if some packages only need to be installed '
                                'with specific options. All commands will be preceded by "pip install"'
                                'On the command-line, the different pip'
                                'commands should be given between quotes, e.g.:\n'
                                '--pip-install "numpy scipy" "pycuda pyvkfft --no-cache-dir" ')
        group.add_argument("--channel", action='store', type=str, nargs='+',
                           help='Conda channels, in addition to the default one and conda-forge. '
                                'This can also be the URL to a channel.')
        group.add_argument('--override-channels', action='store_true',
                           help='Do not search default or .condarc channels. This requires '
                                'using --channel.')
        group.add_argument("--pip-no-cache-dir", action='store_true',
                           help="Add --no-cache-dir to the pip installation command")
        group.add_argument("--pip-no-binary", action='store_true',
                           help="Add --no-binary to the pip installation command")
        group.add_argument("--var-install", action='store', nargs='+',
                           help="Use this to pass environment variables for installation e.g. "
                                "'VKFFT_BACKEND=cuda'")
        group.add_argument("--mamba-timeout", action='store', type=int, default=1200,
                           help='Timeout (s) for the mamba creation/installation and upgrade process')
        group.add_argument("--pip-timeout", action='store', type=int, default=1200,
                           help='Timeout (s) for the pip installation/upgrade process')
        group.add_argument("--distrib", "--distribution", action='store', type=str,
                           help="Distribution name. This is normally automatically detected using "
                                "'lsb_release -is' but this can be used to override this "
                                "value. Useful e.g. for python-only packages which can work "
                                "for multiple distributions, in which case "
                                "'--distrib linux' can be used (using 'linux' automatically "
                                "ignores the release version).")
        group.add_argument("--release", action='store', type=str,
                           help="Release version, e.g. '20.04'. This is normally automatically "
                                "generated using 'lsb_release -rs', but this can be used to "
                                "override this value. Note that this is automatically "
                                "ignored with '--distrib linux'")

        group = parser.add_argument_group("Module parameters")
        parser.add_argument("--maintainer", action='store', type=str,
                            help="Maintainer name + email address (free format, stored in the module)")
        group.add_argument("--prereq", action='store', type=str, nargs='+',
                           help="list of environmental modules required for this module, e.g. 'cuda' "
                                "or 'cuda/11.2' The 'mamba' module is automatically added. These "
                                "will also be loaded before installation.")
        group.add_argument("--conflict", action='store', type=str, nargs='+',
                           help="list of environmental modules conflicting with this module")
        group.add_argument("--set-default", action='store_true',
                           help="Set this module version as the default")

        group = parser.add_argument_group("CVMFS parameters")
        parser.add_argument("--repository", action='store', default="hpc.esrf.fr", type=str,
                            help="CVMFS repository.")
        group.add_argument("--root", action='store', type=str, default='/cvmfs',
                           help="Root cvmfs directory. Default is /cvmfs, other values can be used "
                                "e.g. for testing/development in a local directory. "
                                "Values beginning by / are not allowed.")
        group.add_argument("--cvmfs-timeout", action='store', type=int, default=1200,
                           help='Timeout (s) for the cvmfs transaction commands')
        group.add_argument("--test", action='store_true',
                           help="With this option the program will return successfully after "
                                "parsing the arguments. This is mostly to test if the script "
                                "is correctly installed.")
        self.parser = parser

    def log_start(self):
        self.logger.info(f"Host: {platform.node()}-{platform.platform()}")
        self.logger.info(f"Directory: {os.getcwd()}")
        try:
            self.logger.info(f"User: {os.getlogin()}")
        except OSError:
            # When using slurm, this is not running in a shell with user info
            if 'USER' in os.environ:
                self.logger.info(f"User: {os.environ['USER']}")

        self.logger.info(f"Command-line: \n  {' '.join(sys.argv)}\n")

    def check_input(self):
        # Check package name is not problematic
        name = self.params.name
        if name is None:
            s = "Package name (--name) is missing - aborting"
            self.logger.error(s)
            sys.exit(1)
        version = self.params.version
        if version is None:
            s = "Package version (--version) is missing - aborting"
            self.logger.error(s)
            sys.exit(1)
        if '/' in name + version:
            s = "Please use a package name and version without / - aborting"
            self.logger.error(s)
            sys.exit(1)
        if ' ' in name + version:
            s = "Please use a package name and version without space - aborting"
            self.logger.error(s)
            sys.exit(1)
        if '\000' in name + version:
            s = "Please use a package name and version without \000 - aborting"
            self.logger.error(s)
            sys.exit(1)

        if self.params.maintainer in [None, ''] and not (self.params.upgrade or self.params.delete):
            s = "Please supply a maintainer name and email using --maintainer"
            self.logger.error(s)
            raise RuntimeError(s)
        if self.params.root != '/cvmfs':
            self.logger.warning("Not installing in /cvmfs - no cvmfs command will be issued")
            if self.params.root[0] == '/':
                self.logger.error(f"root directory ({self.params.root}) begins with / but is not /cvmfs - ABORTING")
                sys.exit(1)

        if self.params.override_channels and self.params.channel is None:
            self.logger.error(f"--override-channels is used but not --channel "
                              f"(e.g. don't forget conda-forge) - ABORTING")
            sys.exit(1)

    def make_paths(self):
        params = self.params
        root = params.root
        repository = params.repository
        distrib = self.distrib
        release = self.release
        arch = self.arch
        name = params.name
        version = params.version
        self.paths = {
            'env.full': f"{root}/{repository}/software/packages/{distrib}{release}/{arch}/{name}/{version}",
            'env.root_name': f"{root}/{repository}/software/packages/{distrib}{release}/{arch}/{name}",
            'env.repo_arch': f"{repository}/software/packages/{distrib}{release}/{arch}",
            'env.repo_name': f"{repository}/software/packages/{distrib}{release}/{arch}/{name}",
            'mod.full': f"{root}/{repository}/software/modules/{distrib}{release}/{arch}/{name}/{version}",
            'mod.root_name': f"{root}/{repository}/software/modules/{distrib}{release}/{arch}/{name}",
            'mod.repo_arch': f"{repository}/software/modules/{distrib}{release}/{arch}",
            'mod.repo_name': f"{repository}/software/modules/{distrib}{release}/{arch}/{name}",
        }
        self.logger.info(f"Path to python/conda environment:  \033[1;91m{self.paths['env.full']}\033[0m")
        self.logger.info(f"Path to environmental module:      \033[1;91m{self.paths['mod.full']}\033[0m")
        if ' ' in self.paths['env.full'] + self.paths['mod.full']:
            s = "There is a space in the package and/or module path ! aborting"
            self.logger.error(s)
            sys.exit(1)

    def make_commands(self):
        params = self.params

        if self.params.var_install is not None:
            ev = '; export ' + '; export '.join(self.params.var_install)
        else:
            ev = ''

        module_load = 'mamba'
        if params.prereq is not None:
            module_load += ' ' + ' '.join(params.prereq)
        channels = ' --override-channels' if params.override_channels else ''
        if self.params.channel is not None:
            channels = ' -c ' + ' -c '.join(self.params.channel)

        # List of channels and packages to install and/or update
        cmd1 = f"{channels} --prefix {self.paths['env.full']} "
        if params.conda_install is not None:
            if not params.upgrade and self.params.python is not None:
                cmd1 += f"python={self.params.python} "
            # Add quotes in case < or > is used in version specification,
            # each entry may also include some extra options
            for v in params.conda_install:
                for vv in v.split(' '):
                    if '<' in vv or '>' in vv:
                        cmd1 += f" '{vv}'"
                    else:
                        cmd1 += f" {vv}"
        if params.conda_yaml is not None:
            cmd1 += ' --file '.join([''] + params.conda_yaml)

        # Note 1: this could also be done using the mamba API:
        # https://mamba.readthedocs.io/en/latest/python_api.html?highlight=create#high-level-python-api
        # Note 2: we use --copy to avoid a large number of hardlinks which can create issues with cvmfs
        if params.upgrade:
            if params.conda_yaml is not None:
                # Need to use 'mamba env upgrade' with --file ; 'mamba upgrade...' won't work
                cmd = f"module load {module_load} {ev}; mamba env update --copy {cmd1}"
            else:
                # Install first, then update (updating a non-present package does nothing !)
                cmd = f"module load {module_load} {ev}; mamba --no-banner install -y --copy {cmd1} ;" \
                      f"mamba --no-banner update -y --copy {cmd1}"
        else:
            if params.conda_yaml is not None:
                cmd = f"module load {module_load} {ev}; mamba env create --copy {cmd1}"
            else:
                cmd = f"module load {module_load} {ev}; mamba --no-banner create -y --copy {cmd1}"

        self.logger.info(f"Mamba command:\n  {cmd}\n")
        cmd_pip_v = []

        cmd_pip0 = f"module load {module_load} {ev}; " \
                   f"export LD_LIBRARY_PATH={self.paths['env.full']}/lib:$LD_LIBRARY_PATH ;" \
                   f"export LIBRARY_PATH={self.paths['env.full']}/lib:$LIBRARY_PATH ;" \
                   f"export CPATH={self.paths['env.full']}/include:$CPATH ;" \
                   f"conda activate {self.paths['env.full']} ; "

        if params.pip_install is not None:
            pip_install = []
            for s in params.pip_install:
                if s[:5] == "http:" and s[-7:] in ['.tar.gz', 'tar.bz2']:
                    self.logger.info(f"Pre-downloading {s} to {self.tmp_dir.name}/")
                    # Need to download first, pip does not handle non-https URLs
                    r = subprocess.check_output(f"cd {self.tmp_dir.name} ; curl -O {s}",
                                                shell=True, executable='/bin/bash', stderr=subprocess.STDOUT)
                    tmp = os.path.join(self.tmp_dir.name, os.path.split(urlparse(s).path)[-1])
                    if os.path.exists(tmp):
                        pip_install.append(tmp)
                    else:
                        self.logger.warning(f"Failed to pre-download: {s} => try with original url")
                        pip_install.append(s)
                else:
                    pip_install.append(s)
            cmd_pip = cmd_pip0 + "pip install "
            # Add quotes in case < or > is used in version specification,
            # each entry may also include some extra options
            for v in pip_install:
                for vv in v.split(' '):
                    if '<' in vv or '>' in vv:
                        cmd_pip += f" '{vv}'"
                    else:
                        cmd_pip += f" {vv}"
            if params.pip_no_cache_dir:
                cmd_pip += ' --no-cache-dir'
            if params.pip_no_binary:
                cmd_pip += ' --no-binary'
            if params.upgrade:
                cmd_pip += ' --upgrade'

            cmd_pip_v = [cmd_pip]

        if params.pip_install_sequential is not None:
            for c in params.pip_install_sequential:
                cmd_pip_v.append(f"{cmd_pip0} pip install ")
                for vv in c.split(' '):
                    if '<' in vv or '>' in vv:
                        cmd_pip_v[-1] += f" '{vv}'"
                    else:
                        cmd_pip_v[-1] += f" {vv}"

        for c in cmd_pip_v:
            self.logger.info(f"Pip command:\n  {c}\n")

        if os.path.exists(self.paths['env.full']):
            if params.replace or params.upgrade:
                self.logger.warning("Will remove existing package directory (--replace)")
            else:
                self.logger.error("Package directory already exists! aborting (hint:--replace or --upgrade)")
                sys.exit(1)
        elif params.upgrade:
            self.logger.error("Package directory does not exist but --upgrade is used")
            sys.exit(1)

        return cmd, cmd_pip_v

    def make_module_str(self, package_content):
        params = self.params
        root = params.root
        repository = params.repository
        distrib = self.distrib
        release = self.release
        arch = self.arch
        name = params.name
        version = params.version
        description = params.description
        module_conflict = f'{name}'
        if params.conflict is not None:
            module_conflict += ' ' + ' '.join(params.conflict)
        module_load = 'mamba'
        if params.prereq is not None:
            module_load += ' ' + ' '.join(params.prereq)

        module_str = module_template.format(name=name, version=version, maintainer=params.maintainer,
                                            date=time.strftime("%Y/%m/%d %H:%M:%S"),
                                            distrib=distrib, release=release, arch=arch, root=root,
                                            repository=repository, module_conflict=module_conflict,
                                            module_load=module_load, package_content=package_content,
                                            description=description)
        self.logger.info(f"Module config file:\n\n {module_str}\n")
        return module_str

    def cvmfs_server_transaction(self, path):
        """
        Open a cvmfs server transaction from the given path. If self.params.root does
        not begin with /cvmfs, nothing is done
        :param path: the relative cvmfs path to open
        :return: nothing
        """
        if self.params.root == '/cvmfs':
            self.logger.info(f"Opening cvmfs transaction for: {path}")
            try:
                print(f"cvmfs_server transaction -t {self.params.cvmfs_timeout} {path}")
                r = subprocess.check_output(f"cvmfs_server transaction -t {self.params.cvmfs_timeout} {path}",
                                            shell=True, executable='/bin/bash', stderr=subprocess.STDOUT)
                self.logger.info(r.decode())
            except Exception as ex:
                self.logger.error(ex.output.decode())
                self.logger.error(f"An error occurred initialising cvmfs transaction - aborting: \n"
                                  f"{traceback.format_exc()}\n")
                self.cvmfs_abort()
                exit(1)

    def cvmfs_publish(self):
        if self.params.root == '/cvmfs':
            self.logger.info(f"Publish cvmfs transaction [suppressing hardlink warnings]")
            try:
                # Suppress hardlinks warning messages
                r = subprocess.check_output(f"cvmfs_server publish {self.params.repository}"
                                            f" 2>&1 | grep -v 'We will break up these hardlinks'",
                                            shell=True, executable='/bin/bash', stderr=subprocess.STDOUT)
                self.logger.info(r.decode())
            except Exception as ex:
                self.logger.error(ex.output.decode())
                self.logger.error(f"An error occurred during cvmfs publishing - aborting: \n"
                                  f"{traceback.format_exc()}\n")
                self.cvmfs_abort()
                exit(1)

    def cvmfs_abort(self):
        if self.params.root == '/cvmfs':
            self.logger.info(f"ABORTING: cvmfs_server abort -f {self.params.repository}")
            time.sleep(2)  # Not sure this really helps, in case of sync issue ?
            r = subprocess.check_output(f"cvmfs_server abort -f {self.params.repository}",
                                        shell=True, executable='/bin/bash', stderr=subprocess.STDOUT)
            self.logger.info(r.decode())

    def prepare_package_dir(self):
        # Create parent directory for package if it does not exist
        if not os.path.exists(self.paths['env.root_name']):
            self.logger.info("#" * 80)
            self.logger.info(f"Creating package parent directory: {self.paths['env.root_name']}")
            self.logger.info("#" * 80)
            self.cvmfs_server_transaction(self.paths['env.repo_arch'])
            os.makedirs(self.paths['env.root_name'], exist_ok=True)
            self.cvmfs_publish()

    def make_package(self, cmd, cmd_pip):
        root = self.params.root

        self.logger.info("#" * 80)
        self.logger.info("# Creating or upgrading package")
        self.logger.info("#" * 80)
        self.cvmfs_server_transaction(self.paths['env.repo_name'])

        cmd1, cmd_pip1 = cmd, cmd_pip
        if platform.system() == 'Darwin':
            # macOS does not have module, so deactivate module load
            # for testing
            pat_mod = re.compile(r"module load .*;(.*)")
            cmd1 = pat_mod.match(cmd).group(1)
            if cmd_pip is not None:
                cmd_pip1 = [pat_mod.match(c).group(1) for c in cmd_pip]

        try:
            if self.params.replace and os.path.exists(self.paths['env.full']):
                self.logger.info("#" * 10)
                self.logger.info(f"Removing existing package directory (--replace): {self.paths['env.full']}")
                self.logger.info("#" * 10)
                shutil.rmtree(self.paths['env.full'])

            self.logger.info("#" * 10)
            self.logger.info(f"Creating package directory: {self.paths['env.full']}")
            self.logger.info("#" * 10)
            os.makedirs(self.paths['env.full'], exist_ok=True)
        except Exception as ex:
            self.logger.error(ex.output.decode())
            self.logger.error(f"An error occurred during directory preparation"
                              f" - aborting: \n {traceback.format_exc()}\n")
            self.cvmfs_abort()
            sys.exit(1)

        # Execute - first mamba, then pip
        if not (self.params.upgrade and (self.params.conda_install is None)):
            self.logger.info("#" * 10)
            self.logger.info(f"Mamba command:\n {cmd}", )
            self.logger.info("#" * 10)
            try:
                r = subprocess.check_output(cmd1, shell=True,
                                            executable='/bin/bash', stderr=subprocess.STDOUT,
                                            timeout=self.params.mamba_timeout)
                self.logger.info(r.decode())
            except Exception as ex:
                self.logger.error(ex.output.decode())
                self.logger.error(f"An error occurred during mamba install"
                                  f" - aborting: \n {traceback.format_exc()}\n")
                self.cvmfs_abort()
                sys.exit(1)
        else:
            self.logger.info(f"Upgrading without conda packages listed - skipping mamba install step", )

        if cmd_pip is not None:
            for com in cmd_pip1:
                self.logger.info("#" * 10)
                self.logger.info(f"Pip command:\n {com}")
                self.logger.info("#" * 10)
                try:
                    r = subprocess.check_output(com, shell=True, executable='/bin/bash', stderr=subprocess.STDOUT,
                                                timeout=self.params.pip_timeout)
                    self.logger.info(r.decode())
                except Exception as ex:
                    self.logger.error(ex.output.decode())
                    self.logger.error(f"An error occurred during pip install"
                                      f" - aborting: \n{traceback.format_exc()}\n")
                    self.cvmfs_abort()
                    sys.exit(1)
        else:
            self.logger.info(f"Updating without pip packages listed - skipping pip install", )

        self.logger.info(f"Executing chmod a+rX on package directory")
        try:
            r = subprocess.check_output(f"chmod a+rX {self.paths['env.full']}", shell=True, executable='/bin/bash',
                                        stderr=subprocess.STDOUT, timeout=self.params.pip_timeout)
        except Exception as ex:
            self.logger.error(ex.output.decode())
            self.logger.error(f"An error occurred during chmod - aborting: \n{traceback.format_exc()}\n")
            self.cvmfs_abort()
            sys.exit(1)

        self.logger.info(f"Listing python/conda environment packages")
        try:
            s = "" if platform.system() == 'Darwin' else "module load mamba ;"
            r = subprocess.check_output(f"{s} conda activate {self.paths['env.full']} ; conda list",
                                        shell=True, executable='/bin/bash', stderr=subprocess.STDOUT)
            package_content = r.decode()
        except Exception as ex:
            self.logger.error(ex.output.decode())
            self.logger.error(f"An error occurred when listing package contents"
                              f" - aborting: \n {traceback.format_exc()}\n")
            self.cvmfs_abort()
            exit(1)

        if root == '/cvmfs':
            self.logger.info("#" * 80)
            self.logger.info(f"Publish cvmfs transaction for: {self.paths['env.repo_name']}")
            self.logger.info("# This can take a while - BE PATIENT")
            self.logger.info("#" * 80)
            self.cvmfs_publish()
        return package_content

    def make_module(self, mod_str):
        self.logger.info("#" * 80)
        self.logger.info("# Creating or updating module file")
        self.logger.info("#" * 80)
        try:
            self.cvmfs_server_transaction(self.paths['mod.repo_arch'])
            os.makedirs(self.paths['mod.root_name'], exist_ok=True)
            self.logger.info(f"Creating module:  {self.paths['mod.full']}")
            open(self.paths['mod.full'], mode='w').write(mod_str)
            self.cvmfs_publish()
        except Exception as ex:
            self.logger.error(ex.output.decode())
            self.logger.error(f"An error occurred - aborting: \n{traceback.format_exc()}\n")
            self.cvmfs_abort()
            sys.exit(1)
        if self.params.set_default:
            self.logger.info(f"Setting this module as default")
            self.cvmfs_server_transaction(self.paths['mod.repo_name'])
            open(self.paths['mod.root_name'] + '/.version', mode='w').write(
                f'#%Module - {self.params.name}\n\nset ModulesVersion "{self.params.version}"')
            self.cvmfs_publish()

    def save_log(self):
        if self.params.upgrade:
            logfile = f"{self.paths['env.full']}/upgrade.log"
        else:
            logfile = f"{self.paths['env.full']}/install.log"
        self.logger.info(f"Saving log to: {logfile}")

        self.cvmfs_server_transaction(self.paths['env.repo_name'])
        with open(logfile, 'w') as lf:
            lf.write(self.stream.getvalue())
        self.cvmfs_publish()

    def run(self):
        self.log_start()
        self.check_input()
        self.make_paths()
        if self.params.delete:
            if self.params.upgrade or (self.params.pip_install is not None) or \
                    (self.params.conda_install is not None):
                self.logger.info("Cannot use --delete with --conda-install or --pip-install. Aborting")
                exit()
            if not os.path.exists(self.paths['mod.full']):
                self.logger.info(f"Module path ({self.paths['mod.full']}) does not exist. Aborting")
                exit()
            if not os.path.exists(self.paths['env.full']):
                self.logger.info(f"Package path ({self.paths['env.full']}) does not exist. Aborting")
                exit()
            # Check maintainer name is correct (as a precaution)
            if f"Author: {self.params.maintainer}" not in open(self.paths['mod.full']).read():
                self.logger.info(f"Maintainer name {self.params.maintainer} is not exactly as in the module "
                                 f"description ({self.paths['mod.full']}). This is required as a precaution "
                                 f"for --delete. Aborting")
                exit()
            cmdm = f"rm -Rf {self.paths['mod.full']} "
            cmdp = f"rm -Rf {self.paths['env.full']}"
            self.logger.info("")
            self.logger.info(f"The following command will be issued to delete the module and package:")
            self.logger.info(f"   \033[1;91m{cmdm}\033[0m")
            self.logger.info(f"   \033[1;91m{cmdp}\033[0m")
            r = input("Enter YES to proceed:")
            if r != "YES":
                self.logger.info("Aborting from user input")
                exit()

            self.cvmfs_server_transaction(self.paths['mod.repo_name'])
            self.logger.info(f"Executing:   {cmdm}")
            os.system(cmdm)

            # Check if the deleted module was the default version
            mod_ver = self.paths['mod.root_name'] + '/.version'
            if os.path.exists(mod_ver):
                if f'set ModulesVersion "{self.params.version}"' in open(mod_ver).read():
                    self.logger.warning(f"Deleted module was the default version - removing {mod_ver}")
            self.cvmfs_publish()

            self.cvmfs_server_transaction(self.paths['env.repo_name'])
            self.logger.info(f"Executing:   {cmdp}")
            os.system(cmdp)
            self.cvmfs_publish()
        else:
            cmd, cmd_pip = self.make_commands()

            if not self.params.yes:
                r = input("Enter YES to proceed:")
                if r != "YES":
                    self.logger.info("Aborting from user input")
                    exit()

            self.prepare_package_dir()
            package_content = self.make_package(cmd, cmd_pip)
            module_str = self.make_module_str(package_content)
            self.make_module(module_str)
            self.save_log()


def main():
    CvmfsMkEnv().run()
