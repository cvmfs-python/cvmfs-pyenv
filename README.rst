cvmfs_pyenv
===========
This project provides a simple `cvmfs-pyenv` script for the creation of python environments
installed as `environmental modules <https://modules.readthedocs.io/en/latest/>`_ in the
`ESRF CVMFS repositories <https://confluence.esrf.fr/display/SCKB/Apptainer+-+CVMFS+Software+Provisioning>`_.

The modules are then usable across all TID-managed (Ubuntu) workstations (or more generally any
computer which is a CVMFS client) with the same release and architecture.

Features
========
The environments have the following properties:

* base install of a conda environment using mamba
* any python version supported by conba can be installed
* the conda-forge channel is available by default
* pip-installation is also possible - the pip command is issued after
  the installation of conda packages
* module prereq and conflicts can be listed
* the environmental module is installed automatically
* upgrade of the environment is possible
* distribution (OS) and architecture is automatically recognised

The conda environment is installed in:
    `/cvmfs/REPOSITORY/software/packages/DISTRIBRELEASE/ARCH/NAME/VERSION`

And the environmental module in:
    `/cvmfs/REPOSITORY/software/modules/DISTRIBRELEASE/ARCH/NAME/VERSION`

Where e.g.:

*  REPOSITORY=hpc.esrf.fr
*  DISTRIBRELEASE=ubuntu20.04
*  ARCH=x86_64
*  NAME=pynx
*  VERSION=2022.1-cuda11.2

Use `cvmfs-pyenv --help` for more details

Examples
========
`cvmfs-pyenv --name my_env --version testing --maintainer favre@esrf.fr --python 3.10 --conda-install
pip wheel numpy scipy=1.9.3 matplotlib h5py silx --pip-install pyopencl`

`cvmfs-pyenv --name pynx --version devel-cu11.7 --prereq cuda/11.7 --maintainer favre@esrf.fr --python 3.10
--conda-install pip wheel numpy cython scipy=1.9.3 matplotlib fabio ipympl pytools scikit-image silx
ipython notebook h5py hdf5plugin ipywidgets psutil scikit-learn mako mpi4py pyopengl jupyterlab
ipyvolume ipyfilechooser xraydb pyqt pythreejs=2.3.0 --pip-install h5glance pycuda pyvkfft nabu tomoscan
xrayutilities pyopencl https://gitlab.esrf.fr/favre/PyNX/-/archive/devel/PyNX-devel.tar.bz2`
